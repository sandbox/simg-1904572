Instructions
----------------
1. Install / enable the module

2. Go to input filters and enable "Multiple Content Columns" on any input formats you'd like it to work for

3. Whenever you want to split a content area into multiple columns just enter the string <!-- column break --> in your text

4. You can have as many columns as you like. The module takes care of ensuring they all have the same width.
